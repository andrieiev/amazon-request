class AmazonRequest
  attr_accessor :cookies, :response, :user_agent

  def initialize(email, password)
    @email     = email
    @password  = password
    self.user_agent = 'Mac Safari'
  end

  def request(url)
    signin_if_not
    headers = {
      'User-Agent' => user_agent,
      'Cookie' => HTTP::Cookie.cookie_value(cookies)
    }

    begin
      self.response = RestClient.get(url, headers)
    rescue => e
      self.response = e.response
      return false
    end
  end

  def signin_if_not
    signin if cookies.nil?
  end

  def signin
    signin_page = mechanize_agent.get('https://developer.amazon.com/login.html')

    form          = signin_page.form('signIn')
    form.email    = @email
    form.password = @password

    page = form.submit
    self.cookies = mechanize_agent.cookies
    page
  end

  def mechanize_agent
    @mechanize_agent ||= Mechanize.new.tap do |a|
      a.user_agent_alias    = user_agent
      a.follow_meta_refresh = true
      a.redirect_ok         = true
    end
  end
end
